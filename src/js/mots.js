/**
 * Created by paul on 24/12/2016.
 */

var faults = [];

var score = 0;
var total = 0;
var index;
var max;
var count;
var lap = 0;
var finished = false;

// Initialization
function init() {
    count = 0;
    lap++;
    max = mots.length;
    index = Math.floor(Math.random() * max);
    document.getElementsByClassName("new")[0].innerHTML = mots[index].fr;
    updateScore();
}

function askWord() {
    if (max > 0) max--;
    var last = mots[index];
    mots[index] = mots[max];
    mots[max] = last;

    document.getElementsByClassName("last")[0].id = last.genre;
    document.getElementsByClassName("last")[0].innerHTML = last.fr;
    document.getElementsByClassName("dutch")[0].innerHTML = last.nl;

    if (max > 0) {
        index = Math.floor(Math.random() * max);
        document.getElementsByClassName("new")[0].innerHTML = mots[index].fr;
    } else {
        if (faults.length > 0) {
            mots = faults;
            faults = [];
            init();
        } else {
            finished = true;
            document.getElementsByClassName("new")[0].innerHTML = "FIN";
        }
    }
}

function updateScore() {
    var percent;
    if (total > 0) {
        percent = Math.round(100 * score / total);
    } else {
        percent = 0;
    }
    document.getElementsByClassName("score")[0].innerHTML =  percent + "%";
    document.getElementsByClassName("lap")[0].innerHTML = lap;
    var cnt = count + 1;
    if (cnt >= mots.length) cnt--;
    document.getElementsByClassName("word")[0].innerHTML = cnt + " / " + mots.length;
}

function good() {
    score+=1;
    total++;
    count++;
    updateScore();
    document.getElementsByClassName("result")[0].innerHTML = "VRAI";
    document.getElementsByClassName("result")[0].id = "good";
}

function wrong() {
    total++;
    count++;
    faults.push(mots[index]);
    updateScore();
    document.getElementsByClassName("result")[0].innerHTML = "FAUX";
    document.getElementsByClassName("result")[0].id = "wrong";
}

function testMale() {
    if (finished) return;
    if (mots[index].genre == 'm') {
        good();
    } else {
        wrong();
    }
    askWord();
}

function testFemale() {
    if (finished) return;
    if (mots[index].genre == 'f') {
        good();
    } else {
        wrong();
    }
    askWord();
}

function testNeither() {
    if (finished) return;
    if (mots[index].genre == 'a') {
        good();
    } else {
        wrong();
    }
    askWord();
}
