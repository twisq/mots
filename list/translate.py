#!/usr/bin/python

import sys

print 'var mots = ['
for line in open(sys.argv[1]):
	parts = line.strip().split(',')
	print ' {' + 'fr:"{}", genre:"{}", nl:"{}", date:"{}"'.format(parts[0], parts[1], parts[2], parts[3]) + '},'
print '];'
